var path = require('path');
var express = require('express');
var app = express();

var htmlPath = path.join(__dirname, 'html');

app.use(express.static(htmlPath));

var server = app.listen(80, function () {
    var host = 'localhost';
    var port = server.address().port;
    console.log('listening on http://'+host+':'+port+'/');
});

var key = fs.readFileSync('encryption/private.key');
var cert = fs.readFileSync( 'encryption/certificate.crt' );
var ca = fs.readFileSync( 'encryption/ca_bundle.crt' );

var options = {
  key: key,
  cert: cert,
  ca: ca
};

https.createServer( options, function(req,res)
{
    app.handle( req, res );
} ).listen( 443 );


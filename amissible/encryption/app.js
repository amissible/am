const express = require( 'express' )
    , https = require("https")
    , fs = require( 'fs' );
const exphbs = require('express-handlebars');
const path = require('path');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const redis = require('redis');
const static = require('node-static');
var file = new(static.Server)('./public');


var express_enforces_ssl = require('express-enforces-ssl');

// init routes and middlewares
// Init app
const app = express();
//app.listen( 80 );

var key = fs.readFileSync('encryption/private.key');
var cert = fs.readFileSync( 'encryption/certificate.crt' );
var ca = fs.readFileSync( 'encryption/ca_bundle.crt' );

var options = {
  key: key,
  cert: cert,
  ca: ca
};

https.createServer( options, function(req,res)
{
    app.handle( req, res );
} ).listen( 443 );


// View Engine
app.engine('handlebars',exphbs({defaultLayout:'main'}));
app.set('view engine','handlebars');

// body-parseur
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

// methodOverride
app.use(methodOverride('_method'));

app.get('/',function(req,res,next){
  res.render('searchusers');
});


const http = require('http');
const host = 'api.worldweatheronline.com';
const wwoApiKey = '4009d00d022243d2bd553209182204';
app.post('/weather',(req, res) => {
  // Get the city and date from the request
  let city = req.body.result.parameters['geo-city']; // city is a required param
    console.log('city: ' + req.body.result.parameters['geo-city']);
  // Get the date for the weather forecast (if present)
  let date = '';
  if (req.body.result.parameters['date']) {
    date = req.body.result.parameters['date'];
    console.log('Date: ' + date);
  }
  // Call the weather API
  callWeatherApi(city, date).then((output) => {
    // Return the results of the weather API to Dialogflow
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({ 'speech': output, 'displayText': output }));
  }).catch((error) => {
    // If there is an error let the user know
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({ 'speech': error, 'displayText': error }));
  });
});

app.post('/policy',(req, res) => {
  // Get srchTerm from the request
  console.log('body: ' + JSON.stringify(req.body));
  let srchTerm = req.body.result.parameters['basicTerm'].srchTerm; // city is a required param
  if (typeof srchTerm === 'undefined'){
   srchTerm = req.body.result.parameters['basicTerm'];
  }
    console.log('srchTerm: ' + srchTerm);

  // Call the weather API
  callPolicyApi(srchTerm).then((output) => {
    // Return the results of the weather API to Dialogflow
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(
{ 'speech': output, 'displayText': output,
"messages": {
  "type": 1,
  "title": "card title",
  "subtitle": "card text",
  "imageUrl": "https://assistant.google.com/static/images/molecule/Molecule-Formation-stop.png"
} }
));
  }).catch((error) => {
    // If there is an error let the user know
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({ 'speech': error, 'displayText': error }));
  });
});

function callPolicyApi (srchTerm) {
  return new Promise((resolve, reject) => {
    // Create the path for the HTTP request to get the weather
    let path = '/dtlPolicy/matchingPolicy?' +
      'value=' + encodeURI(srchTerm);
    console.log('API Request: ' + 'localhost' + path);
    // Make the HTTP request to get the weather
    http.get({host: 'localhost', path: path}, (res) => {
      let body = ''; // var to store the response chunks
      res.on('data', (d) => { body += d; }); // store each response chunk
      res.on('end', () => {
        // After all the data has been received parse the JSON for desired data
        let response = JSON.parse(body);
        let reply = response['response'];
        let errMsg = response['errMessage'];

        // Create response
        let output = `${reply}${errMsg}`;
        // Resolve the promise with the output text
        console.log(output);
        resolve(output);
      });
      res.on('error', (error) => {
        reject(error);
      });
    });
  });
};

app.post('/dmv',(req, res) => {
  // Get srchTerm from the request
  //let srchTerm = req.body.result.parameters['srchTerm']; // city is a required param
  console.log('srchTerm: ' + JSON.stringify(req.body));
	
  let srchTerm = req.body.queryResult.parameters['srchTerm']; // city is a required param
  console.log('srchTerm: ' + req.body.queryResult.parameters['srchTerm']);


  // Call the weather API
  callDmvApi(srchTerm).then((output) => {
    // Return the results of the weather API to Dialogflow
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({'fulfillmentText': output}));
  }).catch((error) => {
    // If there is an error let the user know
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({ 'speech': error, 'displayText': error }));
  });
});

function callDmvApi (srchTerm) {
  return new Promise((resolve, reject) => {
    // Create the path for the HTTP request to get the weather
    let path = '/dmv/matchingRules?' +
      'value=' + encodeURI(srchTerm);
    console.log('API Request: ' + 'localhost' + path);
    // Make the HTTP request to get the weather
    http.get({host: 'localhost', path: path}, (res) => {
      let body = ''; // var to store the response chunks
      res.on('data', (d) => { body += d; }); // store each response chunk
      res.on('end', () => {
        // After all the data has been received parse the JSON for desired data
        let response = JSON.parse(body);
        let reply = response['response'];
        let errMsg = response['errMessage'];

        // Create response
        let output = `${reply}${errMsg}`;
        // Resolve the promise with the output text
        console.log(output);
        resolve(output);
      });
      res.on('error', (error) => {
        reject(error);
      });
    });
  });
};


function callWeatherApi (city, date) {
  return new Promise((resolve, reject) => {
    // Create the path for the HTTP request to get the weather
    let path = '/premium/v1/weather.ashx?format=json&num_of_days=1' +
      '&q=' + encodeURI(city) + '&key=' + wwoApiKey + '&date=' + date;
    console.log('API Request: ' + host + path);
    // Make the HTTP request to get the weather
    http.get({host: host, path: path}, (res) => {
      let body = ''; // var to store the response chunks
      res.on('data', (d) => { body += d; }); // store each response chunk
      res.on('end', () => {
        // After all the data has been received parse the JSON for desired data
        let response = JSON.parse(body);
        let forecast = response['data']['weather'][0];
        let location = response['data']['request'][0];
        let conditions = response['data']['current_condition'][0];
        let currentConditions = conditions['weatherDesc'][0]['value'];
        // Create response
        let output = `Current conditions in the ${location['type']}
        ${location['query']} are ${currentConditions} with a projected high of
        ${forecast['maxtempC']}°C or ${forecast['maxtempF']}°F and a low of
        ${forecast['mintempC']}°C or ${forecast['mintempF']}°F on
        ${forecast['date']}.`;
        // Resolve the promise with the output text
        console.log(output);
        resolve(output);
      });
      res.on('error', (error) => {
        reject(error);
      });
    });
  });
};
